# thx-devops-docker

- Simple docker compose starter.
- Requires docker desktop installation : [https://docs.docker.com/desktop/windows/install/]
- Starts a web service (tomcat) on [http://localhost:8080] and a database service (mysql) on [http://localhost:3306]

## Getting started

Navigate to the project folder and execute the following commands:

- Windows:
  - **Start stack:** `docker-compose.exe up --build --detach`
  - Get logs from running stack: `docker-compose.exe logs --follow --tail="all"`
  - Connect bash console to tomcat: `docker-compose.exe exec web bash`
  - Connect bash console to mysql: `docker-compose.exe exec db bash`
  - **Stop stack:** `docker-compose.exe down`

# Folder Structure

``` bash
docker
  tomcat
    Dockerfile # Build file for tomcat
initdb
  test.sql # Sql files executed when mysql starts
webapps # Default tomcat applications
  docs
  examples
  host-manager
  manager
  ROOT # Default root application (used for http://localhost:8080/)
```
