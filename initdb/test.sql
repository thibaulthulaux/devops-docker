CREATE TABLE tasks(
    id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    planning_id int(10)NOT NULL DEFAULT 1, -- TODO: remove default to planning 1 from here and proper assign
    name varchar(50),
    description varchar(255),
    duration varchar(255),
    anteriors varchar(255),
    PRIMARY KEY (id)
);

INSERT INTO tasks (name, description, duration, anteriors)
  VALUES
    ('Task A','Description task A.','3',''),
    ('Task B','Description task B.','4',''),
    ('Task C','Description task C.','5','1,2'),
    ('Task D','Description task D.','2','2'),
    ('Task E','Description task E.','3','2'),
    ('Task F','Description task F.','4','2'),
    ('Task G','Description task G.','5','3,4'),
    ('Task H','Description task H.','6','5'),
    ('Task I','Description task I.','2','6')
  ;